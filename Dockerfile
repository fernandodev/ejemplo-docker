# Obtiene desde el Registry node en su versión 8
# Es la imagen Base
FROM node:8
# Crea una Variable de entorno con un directorio
ENV NODEAPP /nodeapp
# Crea una carpeta llamada nodeapp
RUN mkdir ${NODEAPP}
# Establece el directorio
WORKDIR ${NODEAPP}/
# Copia desde el host al directorio
COPY package.json ${NODEAPP}
# Dentro del contenedor corre el comando e instala dependencias
RUN npm install
# Copia desde el Host el directorio actual al Contenedor
COPY . ${NODEAPP}
# Expone el puerto 8000 del contenedor
EXPOSE 8080
# Define el comando final que se ejecutará
CMD ["npm",  "start"]